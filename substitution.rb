class Substitution < BaseState
  def detect_state
    transition_to(MovingToPuck) unless should_substitute? && can_substitute?
  end

  def _move
    delta =  my_gate_center[0] > rink_center[0] ? 100 : -100
    stop_point = Vector[600 + delta, 165]
    if on_our_side? && my_position[1] < (game.rink_top + game.substitution_area_height)
      if speed_vector.r > game.max_speed_to_allow_substitute
        stop
      else
        set_action(ActionType::SUBSTITUTE)
        moving.teammate_index = best_substitute.teammate_index
      end
    else
      stop_at_point(stop_point)
    end
  end

  def best_substitute
    my_resting.max { |h1, h2| h1.stamina <=> h2.stamina }
  end

end