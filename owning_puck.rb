class OwningPuck < BaseState

  def detect_state
    unless player_own_puck?
      transition_to(MovingToPuck)
      super
    end
  end

  def enter
    unless strategy.hockeyist_own_puck?
      strategy.transition_to(BlockingOpponent)
    end
  end

  def _move
    if blocked?
      if passable?(my_gate_center) && defender_in_place? && distance_to(my_gate_center) > PASS_SAFE_DISTANCE
        pass_to_defender
      end
    end
    if in_strike_position?
      strategy.transition_to(GoalSwing)
    else
      move_to_strike_position
    end
  end

end