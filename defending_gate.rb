class DefendingGate < BaseState
  def detect_state
    if hockeyist_own_puck? || should_take_puck?
      transition_to(MovingToPuck)
    end
    unless opponent_on_our_side? && nearest_to_the_gate?
      transition_to(MovingToPuck)
    end
    if (time_out? || enemy_distance_to_gate > rink_width / 2) && should_substitute? && can_substitute?
      transition_to(Substitution)
    end
  end

  def _move
    my_gate = my_gate_center
    delta = (me.x > my_gate[0] ? DEFENDING_DISTANCE : -DEFENDING_DISTANCE)

    stop_at_point(my_gate + Vector[delta, 0])
    choose_action

    if should_go_out?
      return out_of_the_gate
    end

    if distance_to(puck_vector) < 2 * CLOSE_DISTANCE
      set_turn angle_to(puck_vector)
    end

    return if distance_to(my_gate + Vector[delta, 0]) > VERY_CLOSE_DISTANCE

    if angle_to(my_gate).abs > BaseState::OUR_GATE_SAFE_ZONE
      set_turn angle_to(puck_vector)
    else
      set_turn angle_to(opponent_gate_center)
    end
  end

  def should_go_out?
    my_gate = my_gate_center
    opponent_have_puck? &&
        distance_to(puck_vector) < go_out_distance &&
        nearest_opponent(my_gate[0], my_gate[1]).id == puck_owner.id
  end

  def out_of_the_gate
    move_to_angle(angle_to(puck_vector))
  end

  def choose_action
    if affectable?(puck)
      if opponent_have_puck? || (!puck_speed_suitable? && angle_to(my_gate_center).abs > OUR_GATE_SAFE_ZONE && !goalie_will_take?)
        set_action ActionType::STRIKE
      else
        set_action ActionType::TAKE_PUCK
      end
    end
  end

  def goalie_will_take?
    GameMath.angle_between_vectors(puck_speed_vector, Vector[1,0]).abs < SAFE_ANGLE
  end

end