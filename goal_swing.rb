class GoalSwing < OwningPuck

  def detect_state
    unless in_strike_position? && hockeyist_own_puck?
      transition_to(MovingToPuck)
    end
  end

  def _move
    angle_to_net = angle_to(distant_corner)
    moving.turn = angle_to_net

    if angle_to_net.abs < strike_angle * 2
      pass_to_the_gate
    end
  end

  def enter

  end

end