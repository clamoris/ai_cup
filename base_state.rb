require './state_pattern'
require './helpers'
require './magic'

class BaseState < StatePattern::State
  include Helpers
  include Magic

  # Инициализируем переменные
  def init(me, world, game, move)
    strategy.me = me
    strategy.world = world
    strategy.game = game
    strategy.moving = move
  end

  def move(me, world, game, move)
    strategy.init(me, world, game, move)
    strategy.detect_state
    strategy._move
  end

  def me
    strategy.me
  end

  def world
    strategy.world
  end

  def game
    strategy.game
  end

  def moving
    strategy.moving
  end

  def detect_state
    if should_substitute? && can_substitute?
      transition_to(Substitution)
    elsif should_defend? && !should_take_puck?
      transition_to(DefendingGate)
    end
  end

  def _move
    raise 'This is abstract class, can\'t call _move here'
  end

  def nearest_crossbar
    opponent_player = opponent
    net_x = opponent_player.net_front
    net_y = 0.5 * (opponent_player.net_bottom + opponent_player.net_top)
    net_y += (hockeyist_above?(net_y) ? -0.5 : 0.5) * game.goal_net_height

    Vector[net_x, net_y]
  end

  def distant_corner
    opponent_player = opponent

    net_x = 0.5 * (opponent_player.net_back + opponent_player.net_front)
    net_y = 0.5 * (opponent_player.net_bottom + opponent_player.net_top)

    return Vector[opponent_player.net_front, net_y] if no_goalie?

    net_y += (hockeyist_above?(net_y) ? 0.5 : -0.5) * game.goal_net_height

    Vector[net_x, net_y]
  end

  def closest_corner
    opponent_player = opponent

    net_x = 0.5 * (opponent_player.net_back + opponent_player.net_front)
    net_y = 0.5 * (opponent_player.net_bottom + opponent_player.net_top)

    return Vector[opponent_player.net_front, net_y] if no_goalie?

    net_y += (hockeyist_above?(net_y) ? -0.5 : 0.5) * game.goal_net_height

    Vector[net_x, net_y]
  end

  def near_snout?(object = me, net_vector = distant_corner)
    object.get_distance_to(net_vector[0], net_vector[1]) < snout_distance && !(RINK_CENTER_TOP_BORDER < object.y && RINK_CENTER_BOTTOM_BORDER > object.y)
  end

  def suitable_strike_angle?
    coords = distant_corner

    angle_to_plane = GameMath.angle_between_vectors(Vector[me.x, me.y] - coords, Vector[1, 0]).abs

    if angle_to_plane < PI / 2
      angle_to_plane < MAX_SUCCESSFUL_ANGLE && angle_to_plane > MIN_SUCCESSFUL_ANGLE
    else
      angle_to_plane > (PI - MAX_SUCCESSFUL_ANGLE) && angle_to_plane < (PI - MIN_SUCCESSFUL_ANGLE)
    end
  end

  def suitable_pass_angle?(vector = distant_corner)
    passable?(vector)
  end

  def in_strike_position?
    (near_snout? && suitable_strike_angle?) || no_goalie?
  end

  def pass_to_the_gate
    set_pass_angle(angle_to(distant_corner))
    set_pass_power(1.0)
    set_action(ActionType::PASS)
  end

  def pass_to_defender
    set_pass_angle(angle_to(my_gate_center))
    set_pass_power(0.8)
    set_action(ActionType::PASS)
  end

  def move_to_strike_position
    move_to_angle(angle_to(strike_position))
  end

  def strike_position
    net_vector = nearest_crossbar
    center_vector = rink_center

    if on_our_side?
      curve_delta_x = me.x < rink_center[0] ?  CURVE_RIGHT_X : CURVE_LEFT_X

      any_opp_top = opponent_hockeyists.any? { |h| h.get_distance_to(curve_delta_x, CURVE_TOP_Y) < CURVE_SAFE_DISTANCE }
      any_opp_bottom = opponent_hockeyists.any? { |h| h.get_distance_to(curve_delta_x, CURVE_BOTTOM_Y) < CURVE_SAFE_DISTANCE }

      if !any_opp_top || distance_to(Vector[curve_delta_x, CURVE_TOP_Y]) < CURVE_SAFE_DISTANCE
        curve_delta_y = CURVE_TOP_Y
      elsif !any_opp_bottom || distance_to(Vector[curve_delta_x, CURVE_BOTTOM_Y]) < CURVE_SAFE_DISTANCE
        curve_delta_y = CURVE_BOTTOM_Y
      else
        curve_delta_y = net_vector[1] < center_vector[1] ? CURVE_TOP_Y : CURVE_BOTTOM_Y
      end

      return Vector[curve_delta_x, curve_delta_y]
    end

    delta_x = net_vector[0] < center_vector[0] ? STRIKE_LEFT_X : STRIKE_RIGHT_X
    delta_y = net_vector[1] < center_vector[1] ? STRIKE_TOP_Y : STRIKE_BOTTOM_Y

    Vector[delta_x, delta_y]
  end

  def nearest_opponent(x, y)
    nearest_opponent = nil
    nearest_opponent_range = 0.0

    world.hockeyists.each do |hockeyist|
      next if teammate?(hockeyist) || goalie?(hockeyist) || without_puck?(hockeyist)
      opponent_range = Math.hypot(x - hockeyist.x, y - hockeyist.y)
      if nearest_opponent.nil? || opponent_range < nearest_opponent_range
          nearest_opponent = hockeyist
          nearest_opponent_range = opponent_range
      end
    end
    nearest_opponent
  end

  def nearest_opponent_to(vector)
    nearest_opponent = nearest_opponent(vector[0], vector[1])
    Vector[nearest_opponent.x, nearest_opponent.y]
  end

  def my_gate_center
    net_x = my_player.net_front
    net_y = 0.5 * (my_player.net_bottom + my_player.net_top)
    Vector[net_x, net_y]
  end

  def opponent_gate_center
    net_x = opponent.net_front
    net_y = 0.5 * (opponent.net_bottom + opponent.net_top)
    Vector[net_x, net_y]
  end

  def my_closest_player_to(vector)
    world.hockeyists.reject {|h| opponent?(h) || goalie?(h) || resting?(h) }.min do |h_a, h_b|
      h_a.get_distance_to(vector[0], vector[1]) <=>  h_b.get_distance_to(vector[0], vector[1])
    end
  end

  def closest_player_to(vector)
    world.hockeyists.reject {|h| goalie?(h) || resting?(h) }.min do |h_a, h_b|
      h_a.get_distance_to(vector[0], vector[1]) <=>  h_b.get_distance_to(vector[0], vector[1])
    end
  end

  def stop(current_speed = speed_vector.r)
    future_speed = current_speed * ( 1 - HOCKEYIST_FRICTION )
    return if future_speed.abs < NEARLY_STOPPED_SPEED

    if future_speed > 0
      if future_speed > speed_down_factor
        set_speed_up -1.0
      else
        set_speed_up -(future_speed / speed_down_factor)
      end
    end

    if future_speed < 0
      if future_speed.abs > speed_up_factor
        set_speed_up 1.0
      else
        set_speed_up (future_speed  / speed_up_factor).abs
      end
    end
  end

  def stop_at_point(vector)
    distance = distance_to(vector)
    current_speed_to_point = GameMath.vector_projection(Vector[me.speed_x, me.speed_y], vector - my_position)
    ticks_to_point = distance/current_speed_to_point
    should_slow = ticks_to_point * game.hockeyist_speed_down_factor < current_speed_to_point

    if distance < VERY_CLOSE_DISTANCE
      stop
    elsif current_speed_to_point > 0 && should_slow
      if distance < CLOSE_DISTANCE && angle_to(vector).abs > Math::PI / 4
        move_to_angle(angle_to(vector), -0.5)
      else
        move_to_angle(angle_to(vector), -1.0)
      end
    else
      if distance < CLOSE_DISTANCE && angle_to(vector).abs > Math::PI / 4
        move_to_angle(angle_to(vector), 0.5)
      else
        move_to_angle(angle_to(vector), 1.0)
      end
    end
  end

  def angle_to(vector)
    me.get_angle_to(vector[0], vector[1])
  end

  def distance_to(vector)
    me.get_distance_to(vector[0], vector[1])
  end

  def set_action(action)
    return moving.action = ActionType::CANCEL_STRIKE if me.remaining_cooldown_ticks < 10 &&
        me.last_action == ActionType::SWING &&
        me.state == HockeyistState::SWINGING &&
        !hockeyist_own_puck?
    moving.action = action
  end

  def move_to_angle(angle, speed_up = 1.0)
    set_speed_up( angle < Math::PI / 2 ? speed_up : -speed_up )
    set_turn(angle)
  end

  def set_turn(turn)
    moving.turn = turn
  end

  def set_speed_up(speed_up)
    moving.speed_up = speed_up
  end

  def set_pass_angle(angle)
    moving.pass_angle = angle
  end

  def set_pass_power(power)
    moving.pass_power = power
  end
    #
    # MAX_SEE_VALUE = 120
    # MAX_AVOID_FORCE = 90
    # def move_to_strike_position
    #   strike_vector = strike_position
    #
    #   dynamic_length = speed_vector.r / max_speed
    #   ahead = my_position + speed_vector.normalize * dynamic_length * MAX_SEE_VALUE
    #
    #   ahead2 = my_position + speed_vector.normalize * dynamic_length * MAX_SEE_VALUE * 0.5
    #   collisions = []
    #
    #   opponent_hockeyists.each do |h|
    #     if Math.hypot(ahead[0] - h.x, ahead[1] - h.y) < 60 || Math.hypot(ahead2[0] - h.x, ahead2[1] - h.y) < 60
    #       collisions << Vector[h.x, h.y]
    #     end
    #   end
    #   if ahead[0] > game.rink_right || ahead[0] < game.rink_left ||
    #       ahead[1] > game.rink_bottom || ahead[1] < game.rink_top
    #     collisions << ahead
    #   end
    #
    #   if collisions.size > 0
    #     collision = collisions.min { |a, b| distance_to(a) <=> distance_to(b) }
    #
    #     avoidance_force = ahead - collision
    #     if avoidance_force.r > 0
    #       avoidance_force = avoidance_force.normalize * MAX_AVOID_FORCE
    #
    #       strike_vector = my_position + avoidance_force
    #
    #       puts my_position
    #       puts collision
    #       puts strike_vector
    #     end
    #   end
    #
    #   angle = angle_to(strike_vector)
    #   set_speed_up( angle < Math::PI / 2 ? 1.0 : -1.0 )
    #   set_turn(angle)
    # end

end