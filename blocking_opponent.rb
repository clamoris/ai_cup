class BlockingOpponent < OwningPuck

  def _move
    gate_center = opponent_gate_center
    nearest_opponent = nearest_opponent(gate_center[0], gate_center[1])

    unless nearest_opponent.nil?
      set_action ActionType::STRIKE if affectable?(nearest_opponent) && !affectable?(puck)
      move_to_angle(me.get_angle_to_unit(nearest_opponent))
    end
  end

  def enter

  end

end