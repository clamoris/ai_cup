class GameMath
  def self.angle_between_vectors(v_a, v_b)
    Math.acos(v_a.normalize.inner_product(v_b.normalize))
  end
  
  def self.rotate_by_angle(vector, angle)
    Vector[vector[0] * Math.cos(angle) - vector[1] * Math.sin(angle), vector[0] * Math.sin(angle) + vector[1] * Math.cos(angle)]
  end

  def self.moving_angle(object, x, y, max_speed)
    position = Vector[object.x, object.y]
    velocity = Vector[object.speed_x, object.speed_y]
    direction = Vector[Math.cos(object.angle), Math.sin(object.angle)]
    target = Vector[x, y]

    optimal_direction = (target - position).normalize * max_speed - velocity
    angle = angle_between_vectors(optimal_direction, direction)

    angle
  end

  def self.vector_projection(vector_a, vector_b)
    vector_a.inner_product(vector_b)/vector_b.r
  end

  def self.lines_intersection(point_a1, point_a2, point_b1, point_b2)
    vector_a = point_a2 - point_a1
    vector_b = point_b2 - point_b1

    k2 = (vector_a[0] * (point_b1[1] - point_a1[1]) + vector_a[1] * (point_a1[0] - point_b1[0])) * 1.0 / (vector_a[1] * vector_b[0] - vector_a[0] * vector_b[1])

    some_x = (point_b1[0] + vector_b[0] * k2).to_i
    some_y = (point_b1[1] + vector_b[1] * k2).to_i

    Vector[some_x, some_y]
  end
end