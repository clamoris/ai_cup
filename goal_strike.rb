class GoalStrike < OwningPuck

  def _move
    set_action ActionType::STRIKE
    transition_to(GoalSwing) unless swinging?(me) && hockeyist_own_puck?
  end

  def enter

  end

end