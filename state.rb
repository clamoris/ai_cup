module StatePattern
  class State
    attr_reader :strategy, :previous_state
    def initialize(strategy, previous_state)
      @strategy = strategy
      @previous_state = previous_state
    end

    def self.state_methods
      public_instance_methods - State.public_instance_methods
    end

    def transition_to(state_class)
      @strategy.transition_to(state_class)
    end

    def enter
    end

    def exit
    end
  end
end