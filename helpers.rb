module Helpers

  # World constants
  def max_speed
    game.hockeyist_max_speed
  end

  def world_height
    world.height
  end

  def world_width
    world.width
  end

  def rink_height
    game.rink_bottom - game.rink_top
  end

  def rink_width
    game.rink_right - game.rink_left
  end

  def rink_center
    Vector[world.width / 2, game.rink_top + (rink_height / 2)]
  end

  def speed_down_factor
    game.hockeyist_speed_down_factor
  end

  def speed_up_factor
    game.hockeyist_speed_up_factor
  end

  def strike_angle
    BaseState::STRIKE_ANGLE
  end

  def snout_distance
    world_width * 0.4
  end

  def go_out_distance
    rink_width * 0.3
  end

  # Puck
  def puck
    world.puck
  end

  def puck_vector
    Vector[puck.x, puck.y]
  end

  def puck_speed_vector
    Vector[puck.speed_x, puck.speed_y]
  end

  def puck_speed
    puck_speed_vector.r
  end

  def player_own_puck?
    puck.owner_player_id == me.player_id
  end

  def hockeyist_own_puck?
    puck.owner_hockeyist_id == me.id
  end

  def opponent_have_puck?
    puck_owner && opponent?(puck_owner)
  end

  def we_have_puck?
    puck_owner && teammate?(puck_owner)
  end

  def puck_owner
    world.hockeyists.detect { |h| h.id == puck.owner_hockeyist_id }
  end

  def puck_speed_suitable?
    puck_speed < BaseState::SUITABLE_PUCK_SPEED
  end

  def puck_close_without_owner?
    puck_owner.nil? && distance_to(puck_vector) < BaseState::CLOSE_DISTANCE
  end

  def enemy_far_and_puck_close?
    puck_owner.nil? && distance_to(nearest_opponent_to(puck_vector)) > BaseState::PICK_PUCK_SAFE_DISTANCE && distance_to(puck_vector) < BaseState::CLOSE_DISTANCE * 2
  end

  def should_take_puck?
    puck_speed_suitable? && (puck_close_without_owner? || enemy_far_and_puck_close?)
  end

  # Players
  def opponent
    world.get_opponent_player
  end

  def my_player
    world.get_my_player
  end

  def opponent_hockeyists
    world.hockeyists.reject { |hockeyist| teammate?(hockeyist) || goalie?(hockeyist) || resting?(hockeyist) }
  end

  def my_hockeyists
    world.hockeyists.reject { |hockeyist| opponent?(hockeyist) || goalie?(hockeyist) || resting?(hockeyist) }
  end

  def my_resting
    world.hockeyists.reject { |hockeyist| opponent?(hockeyist) || goalie?(hockeyist) || !resting?(hockeyist) }
  end

  # World states
  def overtime?
    world.tick > world.tick_count
  end

  def no_goalie?
    overtime? && world.get_my_player.goal_count == 0
  end

  def time_out?
    my_player.just_missed_goal || my_player.just_scored_goal
  end

  # Positions
  def near?(object)
    me.get_distance_to_unit(object) < game.stick_length
  end

  def far?(object)
    !near?(object)
  end

  def hockeyist_above?(y)
    me.y < y
  end

  def affectable?(object)
    return false if object.nil?
    near?(object) && (me.get_angle_to_unit(object)).abs < 0.5 * game.stick_sector
  end

  def passable?(vector)
    angle_to(vector).abs < 0.5 * game.pass_sector
  end

  def no_teammate_affected?
    world.hockeyists.reject {|h| opponent?(h) || goalie?(h) }.any? do |h|
      affectable?(h)
    end
  end

  def nearest_to_the_gate?
    my_closest_player_to(my_gate_center).id == me.id
  end

  def opponent_on_our_side?
    net_vector = my_gate_center
    puck_far_from_our_gate = puck.get_distance_to(net_vector[0], net_vector[1]) < (world.width * 0.9)
    puck_very_close_to_our_gate = puck.get_distance_to(net_vector[0], net_vector[1]) < (world.width * 0.9)
    puck_far_from_our_gate && !we_have_puck? || puck_very_close_to_our_gate

    true
  end

  def closest_enemy_to_gate
    gate_center = my_gate_center
    nearest_opponent(gate_center[0], gate_center[1])
  end

  def enemy_distance_to_gate
    gate_center = my_gate_center
    closest_enemy_to_gate.get_distance_to(gate_center[0], gate_center[1])
  end

  def blocked?
    opponent_hockeyists.any? do |h|
      return false if me.get_distance_to_unit(h) > BaseState::CLOSE_DISTANCE / 2
      opponent_sight = Vector[Math.cos(h.angle), Math.sin(h.angle)]
      in_p = opponent_sight.inner_product(Vector[Math.cos(me.angle), Math.sin(me.angle)])
      in_p < -0.6 ? true : false
    end
  end

  def should_defend?
    opponent_on_our_side? && nearest_to_the_gate? && !(player_own_puck? && hockeyist_own_puck?)
  end

  def defender_in_place?
    my_closest_player_to(my_gate_center).get_distance_to(*my_gate_center) < BaseState::DEFENDING_DISTANCE + 50
  end

  def speed_vector
    Vector[me.speed_x, me.speed_y]
  end

  def my_position
    Vector[me.x, me.y]
  end

  def on_our_side?
    distance_to(my_gate_center) < distance_to(opponent_gate_center)
  end

  # Hockeyist states
  def should_substitute?
    time_out? || me.stamina < BaseState::STAMINA_THRESHOLD
  end

  def can_substitute?
    (time_out? || !hockeyist_own_puck?) && my_resting.any? { |h| h.stamina > me.stamina + BaseState::STAMINA_THRESHOLD }
  end

  def without_puck?(hockeyist)
    hockeyist.state == HockeyistState::KNOCKED_DOWN || hockeyist.state == HockeyistState::RESTING
  end

  def teammate?(hockeyist)
    hockeyist.teammate
  end

  def opponent?(hockeyist)
    !hockeyist.teammate
  end

  def goalie?(hockeyist)
    hockeyist.type == HockeyistType::GOALIE
  end

  def resting?(hockeyist)
    hockeyist.state == HockeyistState::RESTING
  end

  def swinging?(hockeyist)
    hockeyist.state == HockeyistState::SWINGING
  end

  def print_statistic
    puts "Tick   ##{world.tick}"
    puts "State  ##{self.class.to_s.downcase}"
    puts "Coords (#{me.x},#{me.x})"
    puts "Speed  #{speed_vector.r}"

    puts "Time-out #{my_player.just_missed_goal || my_player.just_scored_goal}"

    puts "Puck distance #{distance_to(puck_vector)}"
    puts "Puck speed #{puck_speed}"
    puts "Puck affectable?  #{affectable?(puck)}"
    puts ''
  end

end