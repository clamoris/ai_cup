include Math
require './matrix'
require './model/action_type'
require './model/game'
require './model/move'
require './model/hockeyist'
require './model/world'
require './state_pattern'
require './game_math'
# States
require './base_state'
require './moving_to_puck'
require './owning_puck'
require './blocking_opponent'
require './goal_strike'
require './goal_swing'
require './choosing_strategy'
require './defending_gate'
require './substitution'


class MyStrategy
  # @param [Hockeyist] me
  # @param [World] world
  # @param [Game] game
  # @param [Move] move

  attr_accessor :me, :world, :game, :moving

  include StatePattern
  set_initial_state MovingToPuck

  def initialize

  end

end