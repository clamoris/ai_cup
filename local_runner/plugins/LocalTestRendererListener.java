import java.awt.*;

import model.*;
import static java.lang.StrictMath.*;


public final class LocalTestRendererListener {
    public void beforeDrawScene(Graphics graphics, World world, Game game, double scale) {


        graphics.setColor(new Color(240, 240, 240));

        for (Hockeyist h : world.getHockeyists()) {
            int ang = (int) (h.getAngle() * 180 / PI);
            graphics.fillArc((int) h.getX() - 120, (int) h.getY() - 120, 240, 240, -ang - 60, 120);
        }

        graphics.setColor(new Color(220, 220, 220));
                for (Hockeyist h : world.getHockeyists()) {
                    int ang = (int) (h.getAngle() * 180 / PI);
                    graphics.fillArc((int) h.getX() - 120, (int) h.getY() - 120, 240, 240, -ang - 15, 30);
                }

        graphics.setColor(new Color(0, 0, 0));

        graphics.drawRect(200, 250, 300, 130);
        graphics.drawRect(700, 250, 300, 130);

        graphics.drawRect(200, 540, 300, 130);
        graphics.drawRect(700, 540, 300, 130);

        graphics.setColor(new Color(50, 0, 0));

        graphics.drawRect(230, 280, 240, 100);
        graphics.drawRect(730, 280, 240, 100);

        graphics.drawRect(230, 540, 240, 100);
        graphics.drawRect(730, 540, 240, 100);


        graphics.setColor(new Color(220, 220, 220));

        graphics.fillArc(32 - 420, 360 - 420, 840, 840, -51, 25);
        graphics.fillArc(32 - 420, 560 - 420, 840, 840, 25, 25);
        graphics.fillArc(1168 - 420, 360 - 420, 840, 840, -154, 25);
        graphics.fillArc(1168 - 420, 560 - 420, 840, 840, 128, 25);

        graphics.setColor(new Color(0, 0, 0));

        graphics.drawRect(680, 690, 1, 1);
        graphics.drawRect(680, 230, 1, 1);
        graphics.drawRect(520, 690, 1, 1);
        graphics.drawRect(520, 230, 1, 1);

        graphics.drawRect(270, 560, 1, 1);
        graphics.drawRect(930, 560, 1, 1);
        graphics.drawRect(270, 360, 1, 1);
        graphics.drawRect(930, 360, 1, 1);
    }

    public void afterDrawScene(Graphics graphics, World world, Game game, double scale) {

    }
}
