class MovingToPuck < BaseState

  def detect_state
    if player_own_puck?
      transition_to(OwningPuck)
    else
      super
    end
  end

  def distance_to_puck
    me.get_distance_to(puck.x, puck.y)
  end

  def _move
    my_speed_to_puck = GameMath.vector_projection(Vector[me.speed_x, me.speed_y], puck_vector - my_position)
    puck_speed_to_me = GameMath.vector_projection(puck_speed_vector, my_position - puck_vector)

    speed_to_puck = my_speed_to_puck + puck_speed_to_me
    speed_to_puck = 2 if speed_to_puck < 2
    future_position = puck_future_position(distance_to_puck / speed_to_puck)
    move_to_angle(angle_to(future_position))

    if puck_speed_vector.r > 0 &&
        (GameMath.angle_between_vectors(puck_speed_vector, distant_corner - puck_vector).abs < 0.1 ||
        GameMath.angle_between_vectors(puck_speed_vector, closest_corner - puck_vector).abs < 0.1) &&
        ((distant_corner - puck_vector).r < rink_width * 0.3 || (closest_corner - puck_vector).r < rink_width * 0.3)
      return set_action ActionType::NONE
    end
    if can_strike_goal? || should_clear?
      set_action ActionType::STRIKE
    else
      set_action ActionType::TAKE_PUCK
    end
  end

  def can_strike_goal?
    affectable?(puck) && in_strike_position? && angle_to(distant_corner).abs < 0.05
  end

  def should_clear?
    affectable?(puck) &&
        opponent_have_puck? &&
        angle_to(my_gate_center).abs > BaseState::OUR_GATE_SAFE_ZONE &&
        puck_owner.get_angle_to(my_gate_center[0], my_gate_center[1]) < Math::PI / 2
  end

  def opponent_in_our_snout?
    near_snout?(puck_owner, my_gate_center)
  end

  def puck_future_position( n = 10 )
    return puck_vector if distance_to(puck_vector) < CLOSE_DISTANCE
    return puck_owner_position(n) if puck_owner
    future_position = puck_vector
    puck_speed = puck_speed_vector
    n.to_i.times do
      angle = intersection_angle(future_position)
      if angle
        puck_speed *= (1 - WALL_SPEED_REDUCTION)
        puck_speed = GameMath.rotate_by_angle(puck_speed, 2 * angle)
      else
        puck_speed *= (1 - PUCK_FRICTION)
      end
      future_position += puck_speed
    end
    future_position
  end

  def puck_owner_position(n = 10)
    return puck_future_position(n) unless puck_owner
    speed = Vector[puck_owner.speed_x, puck_owner.speed_y]
    puck_delta = Vector[Math.cos(puck_owner.angle), Math.sin(puck_owner.angle)] * PUCK_DISTANCE_TO_OWNER
    Vector[puck_owner.x, puck_owner.y] + speed * n + puck_delta
  end

  def intersection_angle(vector)
    return false if vector == puck_vector
    if vector[0] + 20 >= game.rink_right || vector[0] - 20 <= game.rink_left
      return GameMath.angle_between_vectors(vector - puck_vector, Vector[0, 1])
    end
    if vector[1] - 20 <= game.rink_top || vector[1] + 20 >= game.rink_bottom
      return GameMath.angle_between_vectors(vector - puck_vector, Vector[1, 0])
    end
    false
  end


  def intersection_point(vector)
    return false if vector == puck_vector
    if vector[0] + 20 >= game.rink_right
      return GameMath.lines_intersection(puck_vector, vector, Vector[game.rink_right, 0], Vector[game.rink_right, 1])
    end
    if vector[0] - 20 <= game.rink_left
      return GameMath.lines_intersection(puck_vector, vector, Vector[game.rink_left, 0], Vector[game.rink_left, 1])
    end
    if vector[1] - 20 <= game.rink_top
      return GameMath.lines_intersection(puck_vector, vector, Vector[0, game.rink_top], Vector[1, game.rink_top])
    end
    if vector[1] + 20 >= game.rink_bottom
      return GameMath.lines_intersection(puck_vector, vector, Vector[0, game.rink_bottom], Vector[1, game.rink_bottom])
    end
    false
  end

end